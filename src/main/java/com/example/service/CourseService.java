package com.example.service;

import com.example.model.Course;

public interface CourseService {
    Course findCourseById(Integer id);

    Course addCourse(Course course);

    Course updateCourse(Course course);

    void deleteCourse(Integer id);

}
