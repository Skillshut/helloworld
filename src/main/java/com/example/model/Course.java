package com.example.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "courses")
public class Course {

    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_course")
    private Integer id;
    @Column(name = "name_course")
    private String name;
    @Column(name = "description_course")
    private String description;
    @Column(name = "difficulty_course")
    private Integer difficulty;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_topic")
    private Topic topic;

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public Course() {

    }

    public Course(String name, String description, Integer difficulty) {
        this.name = name;
        this.description = description;
        this.difficulty = difficulty;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Integer difficulty) {
        this.difficulty = difficulty;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Course course = (Course) o;
        return Objects.equals(id, course.id) && Objects.equals(name, course.name) && Objects.equals(description, course.description) && Objects.equals(difficulty, course.difficulty) && Objects.equals(topic, course.topic);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, difficulty, topic);
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", difficulty=" + difficulty +
                ", topic=" + topic +
                '}';
    }
}
