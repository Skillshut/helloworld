package com.example.controller;

import com.example.model.Course;
import com.example.model.Topic;
import com.example.service.impl.CourseServiceImpl;
import com.example.service.impl.TopicServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/topics")
public class TopicController {


    private final TopicServiceImpl topicServiceImpl;

    @Autowired
    public TopicController(TopicServiceImpl topicServiceImpl) {
        this.topicServiceImpl = topicServiceImpl;
    }


    @GetMapping("/{id}")//foo
    public ResponseEntity get(@PathVariable Integer id) {//если имена не совпадают добавить - @PathVariable("foo")
        if (id == null)
            return new ResponseEntity(HttpStatus.BAD_REQUEST);

        return Optional.ofNullable(topicServiceImpl.findTopicById(id))
                .map(topic -> new ResponseEntity(topic, HttpStatus.OK))
                .orElse(new ResponseEntity(HttpStatus.NOT_FOUND));

    }

    @PostMapping
    public ResponseEntity add(@RequestBody Topic topic) {
        return Optional.ofNullable(topicServiceImpl.addTopic(topic))
                .map(t -> new ResponseEntity(t, HttpStatus.OK))
                .orElse(new ResponseEntity(HttpStatus.BAD_REQUEST));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Integer id) {
        return Optional.ofNullable(id)
                .map(i -> {
                    topicServiceImpl.deleteTopic(id);
                    return new ResponseEntity(HttpStatus.OK);
                })
                .orElse(new ResponseEntity(HttpStatus.BAD_REQUEST));
    }

    //проверка id может нужна
    @PutMapping("/{id}")
    public ResponseEntity update(@RequestBody Topic topic, @PathVariable Integer id) {
        return Optional.ofNullable(topic)
                .map(t -> {
                    topicServiceImpl.updateTopic(t, id);
                    return new ResponseEntity(t, HttpStatus.OK);
                })
                .orElse(new ResponseEntity(HttpStatus.OK));
    }

    @GetMapping
    public List<Topic> getAllTopics() {
        return topicServiceImpl.getAllTopics();
    }


}
