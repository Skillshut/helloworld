package com.example.controller;

import com.example.model.Course;
import com.example.model.Topic;
import com.example.service.impl.CourseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.Optional;

@RestController
@RequestMapping("/courses")
public class CourseController {

    private final CourseServiceImpl courseServiceImpl;

    @Autowired
    public CourseController(CourseServiceImpl courseServiceImpl) {
        this.courseServiceImpl = courseServiceImpl;
    }

    @PostMapping
    public ResponseEntity add(@RequestBody Course course) {

        return Optional.ofNullable(courseServiceImpl.addCourse(course))
                .map(c -> new ResponseEntity(c, HttpStatus.CREATED))
                .orElse(new ResponseEntity(HttpStatus.BAD_REQUEST));
    }

    @GetMapping("/{id}")
    public ResponseEntity get(@PathVariable(value = "id") Integer courseId/*, @RequestParam(value = "topicId") Integer topicId*/) {
//        if (topicId == null)
//            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        return Optional.ofNullable(courseServiceImpl.findCourseById(courseId))
                .map(c -> new ResponseEntity(c, HttpStatus.OK))
                .orElse(new ResponseEntity(HttpStatus.BAD_REQUEST));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Integer id) {
        return Optional.ofNullable(id)
                .map(i -> {
                    courseServiceImpl.deleteCourse(id);
                    return new ResponseEntity(HttpStatus.OK);
                })
                .orElse(new ResponseEntity(HttpStatus.BAD_REQUEST));
    }

    @PutMapping
    public ResponseEntity update(@RequestBody Course course) {
        return Optional.ofNullable(course)
                .map(t -> {
                    courseServiceImpl.updateCourse(t);
                    return new ResponseEntity(t, HttpStatus.OK);
                })
                .orElse(new ResponseEntity(HttpStatus.OK));
    }
}
