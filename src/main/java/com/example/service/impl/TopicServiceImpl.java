package com.example.service.impl;

import com.example.model.Course;
import com.example.model.Topic;
import com.example.repository.TopicRepository;
import com.example.service.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class TopicServiceImpl implements TopicService {


    private final TopicRepository topicRepository;

    @Autowired
    public TopicServiceImpl(TopicRepository topicRepository) {
        this.topicRepository = topicRepository;
    }

    @Override
    @Transactional
    public Topic findTopicById(Integer id) {
        return topicRepository.findTopicById(id);
    }

    @Override
    @Transactional
    public Topic addTopic(Topic topic) {
        return topicRepository.save(topic);
    }

    @Override
    @Transactional
    public Topic updateTopic(Topic topic, Integer id) {
        topic.setId(id);
        return topicRepository.save(topic);
    }

    @Override
    @Transactional
    public void deleteTopic(Integer id) {
        topicRepository.deleteById(id);
    }

    @Override
    @Transactional
    public List<Topic> getAllTopics() {
        return topicRepository.findAll();
    }

}