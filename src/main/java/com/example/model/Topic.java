package com.example.model;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;


@Entity
@Table(name = "topics")
public class Topic {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_topic")
    private Integer id;
    @Column(name = "name_topic")
    private String name;
    @Column(name = "description_topic")
    private String description;

    @OneToMany(mappedBy = "topic")
    private List<Course> courses;

    public void addCourse(Course course) {
        this.courses.add(course);
    }

    public Optional<Course> getCourse(Integer id) {
        return this.courses.stream().filter(c -> c.getId().equals(id)).findFirst();
    }

    public void removeCourse(Course course) {
        this.courses.remove(course);
    }

    public Topic() {

    }

    public Topic(Integer id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
        courses = new ArrayList<>();
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Topic topic = (Topic) o;
        return Objects.equals(id, topic.id) && Objects.equals(name, topic.name) && Objects.equals(description, topic.description) && Objects.equals(courses, topic.courses);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, courses);
    }

    @Override
    public String toString() {
        return "Topic{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", courses=" + courses +
                '}';
    }
}
