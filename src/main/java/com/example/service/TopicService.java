package com.example.service;

import com.example.model.Topic;

import java.util.List;
import java.util.Optional;

public interface TopicService {

    Topic findTopicById(Integer id);

    Topic addTopic(Topic topic);

    Topic updateTopic(Topic topic, Integer id);

    void deleteTopic(Integer id);

    List<Topic> getAllTopics();
}
